import React from "react";
import NavBar from "../Component/NavBar";
import PetheanImg from "../Assets/Images/petheanrajimage.jpg";
import Index from "../Router/Index";

const Layout = () => {
  return (
    <div>
      <NavBar />
      <div className="content m-16 flex">
        <img class="rounded-lg" src={PetheanImg} alt="PetheanImage" />
        <Index />
      </div>
    </div>
  );
};

export default Layout;
