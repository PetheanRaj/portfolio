import React, { useEffect } from 'react'
import NavBar from '../Component/NavBar'

const Home = () => {
  class TypeWriter {
    constructor(txtElement, words, wait = 3000) {
      this.txtElement = txtElement;
      this.words = words;
      this.txt = "";
      this.wordIndex = 0;
      this.wait = parseInt(wait, 10);
      this.type();
      this.isDeleting = false;
    }

    type() {
      // Current index of word
      const current = this.wordIndex % this.words.length;
      // Get full text of current word
      const fullTxt = this.words[current];

      // Check if deleting
      if (this.isDeleting) {
        // Remove char
        this.txt = fullTxt.substring(0, this.txt.length - 1);
      } else {
        // Add char
        this.txt = fullTxt.substring(0, this.txt.length + 1);
      }

      // Insert txt into element
      this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

      // Initial Type Speed
      let typeSpeed = 300;

      if (this.isDeleting) {
        typeSpeed /= 2;
      }

      // If word is complete
      if (!this.isDeleting && this.txt === fullTxt) {
        // Make pause at end
        typeSpeed = this.wait;
        // Set delete to true
        this.isDeleting = true;
      } else if (this.isDeleting && this.txt === "") {
        this.isDeleting = false;
        // Move to next word
        this.wordIndex++;
        // Pause before start typing
        typeSpeed = 500;
      }

      setTimeout(() => this.type(), typeSpeed);
    }
  }

  // Init On DOM Load
  useEffect(() => {
    init();
  }, []);

  // Init App
  function init() {
    const txtElement = document.querySelector(".txt-type");
    const words = JSON.parse(txtElement.getAttribute("data-words"));
    const wait = txtElement.getAttribute("data-wait");
    // Init TypeWriter
    new TypeWriter(txtElement, words, wait);
  }
  return (
    <>
        <div className='profilename my-auto ml-8'>
          <h1 class="text-4xl ... font-bold">PETHEAN RAJ A</h1>
          <p class="h-1.5 w-16 bg-stone-900"></p>
          <div className='flex flex-direction: row;'>
          <h3 class="text-gray-700 text-2xl">Creative</h3>
          <h1
              data-wait="2000"
              data-words='["FreeLancer","Designer"]'
              className="txt-type text-gray-700 text-2xl"
            ></h1>
            </div>
            <button className='px-8 h-12 mt-5 text-white bg-slate-800 hover:text-black hover:border-solid hover:border-2 hover:border-black hover:bg-white'>Get in Touch</button>
        </div>
</>
  )
}

export default Home