import React from 'react'
import { Route, BrowserRouter, Routes } from 'react-router-dom'
import About from '../Pages/About'
import Home from '../Pages/Home'

const Index = () => {
  return (
<BrowserRouter>
<Routes>
    <Route exact path='/' element={<Home/>} /> 
    <Route exact path='/About' element={<About/>} />
    </Routes>
    </BrowserRouter>
  )
}

export default Index