import React from "react";
import { Link } from "react-router-dom";
import Logo from "../Assets/Images/Logofinal.png";

const Header = () => {
  // function navabout(){
  //   window.location.replace("/About")
  // }
  // function navhome(){
  //   window.location.replace("/")
  // }
  return (

    <div>
      <div className="flex justify-between mx-16">
        <img src={Logo} alt="Logo" />
        <div class="flex">
          <Link to="/" class="px-8 transition transform delay-150 hover:-translate-y-1 hover:scale-110 hover:text-white hover:bg-slate-800 duration-300 ...">
            Home
          </Link>
        <Link to="/About" class="px-8 transition transform delay-150 hover:-translate-y-1 hover:scale-110 hover:text-white hover:bg-slate-800 duration-300 ...">
          About
        </Link>
          <button class="px-8 transition transform delay-150 hover:-translate-y-1 hover:scale-110 hover:text-white hover:bg-slate-800 duration-300 ...">
            Portfolio
          </button>
          <button class="px-8 transition transform delay-150 hover:-translate-y-1 hover:scale-110 hover:text-white hover:bg-slate-800 duration-300 ...">
            Service
          </button>
          <button class="px-8 transition transform delay-150 hover:-translate-y-1 hover:scale-110 hover:text-white hover:bg-slate-800 duration-300 ...">
            News
          </button>
          <button class="px-8 transition transform delay-150 hover:-translate-y-1 hover:scale-110 hover:text-white hover:bg-slate-800 duration-300 ...">
            Contact
          </button>
        </div>
      </div>
    </div>
  );
};

export default Header;
